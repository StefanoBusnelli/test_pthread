#include <pthread.h>
#include <stdlib.h>
#include <stdio.h>

void *counter( void *thid ) {
  int i = 0;

  printf( "counter %d\n", ( int *)thid );
  for ( i=0; i<1000; i++ ) {
    printf( " %d: %d\n", (int *)thid, i );
  }
  
  pthread_exit( NULL );
}

int main( char *argc, char** argv ) {
  pthread_t            *thread_0= NULL;
  pthread_t            *thread_1= NULL;
  pthread_attr_t	attr;
  void                 *status  = NULL;
  int                   rc      = 0;

  thread_0 = ( pthread_t * ) malloc( sizeof(  pthread_t ) );
  thread_1 = ( pthread_t * ) malloc( sizeof(  pthread_t ) );

  pthread_attr_init( &attr );
  pthread_attr_setdetachstate( &attr, PTHREAD_CREATE_JOINABLE );

  rc = pthread_create( thread_0, &attr, counter, ( void *)0 );
  if ( rc != 0 ) {
    printf( "Non posso creare thread 0\n" );
    exit -1;
  }
  rc = pthread_create( thread_1, &attr, counter, ( void *)1 );
  if ( rc != 0 ) {
    printf( "Non posso creare thread 1\n" );
    exit -1;
  }

  pthread_attr_destroy( &attr );

  rc = pthread_join( *thread_0, &status );
  if ( rc != 0 ) {
    printf( "Errore join thread 0\n" );
    exit -1;
  }
  printf( "Thread 0 ha terminato\n" );

  rc = pthread_join( *thread_1, &status );
  if ( rc != 0 ) {
    printf( "Errore join thread 1\n" );
    exit -1;
  }
  printf( "Thread 1 ha terminato\n" );

  pthread_exit( NULL );
}
