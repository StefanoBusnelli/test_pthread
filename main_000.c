#include <pthread.h>
#include <stdlib.h>
#include <stdio.h>

void *counter( void *thid ) {
  int i = 0;

  printf( "counter %d\n", ( int *)thid );
  for ( i=0; i<1000; i++ ) {
    printf( " %d: %d\n", (int *)thid, i );
  }
  
  pthread_exit( NULL );
}

int main( char *argc, char** argv ) {
  pthread_t    *thread_0, *thread_1;
  int           rc      = 0;

  thread_0 = ( pthread_t * ) malloc( sizeof(  pthread_t ) );
  thread_1 = ( pthread_t * ) malloc( sizeof(  pthread_t ) );

  rc = pthread_create( thread_0, NULL, counter, ( void *)0 );
  if ( rc != 0 )
    printf( "Non posso creare thread 0\n" );
  rc = pthread_create( thread_1, NULL, counter, ( void *)1 );
  if ( rc != 0 )
    printf( "Non posso creare thread 1\n" );

  pthread_exit( NULL );
}
