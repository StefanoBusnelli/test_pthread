CC=gcc

all: test_000 test_001 test_002 test_003

test_000: main_000.c
	$(CC) -o bin/$@ -lpthread $^

test_001: main_001.c
	$(CC) -o bin/$@ -lpthread $^

test_002: main_002.c
	$(CC) -o bin/$@ -lpthread $^

test_003: main_003.c
	$(CC) -o bin/$@ -lpthread $^

clean:
	rm -f *.o test_???
