/*
	Lancio 2 thread che controllano un flag presente in una struct condivisa,
		se il flag vale 0 i thread terminano.
	Imposto il falore del flag nel thread principale.
*/
#include <pthread.h>
#include <stdlib.h>
#include <stdio.h>

typedef struct t_appl t_appl;
typedef struct t_appl {
  char	f_state;
} tt_appl;

void *f_thread( void *appl ) {

  printf( "\nf_thread start" );
  while ( (( t_appl *)appl)->f_state != 0 ) {
    ;
  }
  printf( "\nf_thread end" );

  pthread_exit( NULL );
}

int main( char *argc, char** argv ) {
  pthread_t            *thread_0= NULL;
  pthread_t            *thread_1= NULL;
  pthread_attr_t	attr;
  void                 *status  = NULL;
  int                   rc      = 0;
  unsigned char         cmd     = '\0';
  t_appl                appl;

  appl.f_state = 1;

  thread_0 = ( pthread_t * ) malloc( sizeof(  pthread_t ) );
  thread_1 = ( pthread_t * ) malloc( sizeof(  pthread_t ) );

  pthread_attr_init( &attr );
  pthread_attr_setdetachstate( &attr, PTHREAD_CREATE_JOINABLE );

  rc = pthread_create( thread_0, &attr, f_thread, ( void *)&appl );
  if ( rc != 0 ) {
    printf( "\nNon posso creare thread 0" );
    exit -1;
  }
  rc = pthread_create( thread_1, &attr, f_thread, ( void *)&appl );
  if ( rc != 0 ) {
    printf( "\nNon posso creare thread 1" );
    exit -1;
  }

  pthread_attr_destroy( &attr );

  while( cmd != '0' ) {
    printf( "\nComando:\n 0: Exit\n :" );
    cmd = getchar();
    if ( cmd == '0' )
      appl.f_state = 0;
  }

  rc = pthread_join( *thread_0, &status );
  if ( rc != 0 ) {
    printf( "\nErrore join thread 0" );
    exit -1;
  }
  printf( "\nThread 0 ha terminato" );

  rc = pthread_join( *thread_1, &status );
  if ( rc != 0 ) {
    printf( "\nErrore join thread 1" );
    exit -1;
  }
  printf( "\nThread 1 ha terminato" );

  printf( "\nFine\n" );

  pthread_exit( NULL );
}
